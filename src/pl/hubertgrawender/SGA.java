package pl.hubertgrawender;

import java.util.*;
import java.text.DecimalFormat;

public class SGA {
    
    public static List<String> gen_rand_inputs( int x_no, int bit_no){
        List<String> inputs = new ArrayList<>();
        
        for (int i = 0; i < x_no; i++){
            String tmp="";
            
            for (int j = 0; j < bit_no; j++){
                tmp += rand_bin();
            }
            inputs.add(tmp);
        }
        return inputs;
    }
    
    public static int decode_bin(String p_number) {
        int decimalValue = Integer.parseInt(p_number, 2);
        return decimalValue;
    }
    

    public static double calc_log( double a, double b ) {
        return Math.log(a) / Math.log(b);
    }

    public static int calculate_number_of_bits(int precision, double max, double min){
        double logarithm = calc_log((max - min)*Math.pow(10,precision),2);
        double result = Math.ceil(logarithm+1) ;
        return (int) result;
    }

    public static List<Double> create_chromosomes(List<String> input, int precision, int bit_number, double max, double min){
        List<Double> chromosomes = new ArrayList<>();
        int decoded_number;
        double result;
        double result_dot;
        String p = "";
        for (int i=0;i<precision;i++){
            p+="#";
        }
        
        String pattern = "#."+p;
        for (int i=0;i<input.size();i++){
            decoded_number = decode_bin(input.get(i));
            result = min + (( decoded_number*(max-min))  / (Math.pow(2,bit_number) - 1 ) );
            DecimalFormat df = new DecimalFormat(pattern);
            result_dot = Double.parseDouble(df.format(result).replace(",", "."));
            chromosomes.add(result_dot);
        }
        return chromosomes;
    }
    
    public static List<Double> calc_fitness_fun(List<Double> list){
        List<Double> new_list = new ArrayList<>();
        
        for (int i=0;i<list.size();i++){
            new_list.add(FIT.fitness_fun(list.get(i)));
        }
        return new_list;
    }

    public static Double fitness_sum(List<Double> list){
        Double result=0.0;
        for (int i=0;i<list.size();i++){
            result+= list.get(i);
        }
        return result;
    }
    

    public static List<Double> create_roulette(List<Double> input, double sum){
        List<Double> selection_prob = new ArrayList<>();

        double result=0.0;
        for (int i=0;i<input.size();i++){
           // System.out.println("INPUT: "+input.get(i));
            result = input.get(i)/sum;
            selection_prob.add(result);
        }
        return selection_prob;
    }

    public static int getRandomInput(List<Double> input,int maxsize){
        int result=0;
        double random = rand_double(1.0,0.0);
        int i=0;
        double tmp=0.0;
        while (true){
            if(i==0){
                tmp += input.get(i);
                if (tmp>random){
                    result = i;
                    break;
                }
            }
            tmp += input.get(i);
            if ( tmp > random) {
                result = i+1;
                break;
            }
            i++;
        }
        if ( result>=(maxsize-1) ) result=(maxsize-1);
        return result;
    }
    

    public static int rand_method(double p_pm, double p_pc, double p_pr){
        double random = SGA.rand_double(1.0,0.0);
        if ( random <= p_pc ) return 0;
        else if (random > p_pc && random < (p_pm+p_pc)) return 1;
        else if (random >= (p_pm+p_pc) && random <=1 ) return 2;
        return 0;
    }

    public static String mutation(String x, int BIT_NUMBER){
        int random = rand_int(BIT_NUMBER-1,0);
        StringBuilder result = new StringBuilder(x);
        if(result.charAt(random)=='0') result.setCharAt(random,'1');
        else if(result.charAt(random)=='1') result.setCharAt(random,'0');
        return result.toString();
    }

    // random between 1 - 7
    public static List<String> crossover(String x1,String x2,int BIT_NUMBER){
        int random =  rand_int(BIT_NUMBER-1,1);
        List<String>  result = new ArrayList<>();
        String tmp1 = x1.substring(0, random);
        String tmp2 = x1.substring(random);
        String tmp3 = x2.substring(0,random);
        String tmp4 = x2.substring(random);

        String result1 = tmp1 + tmp4;
        String result2 = tmp2 + tmp3;

        result.add(result1);
        result.add(result2);
        return result;
    }

    public static String reproduction(String x){
        String result = x;
        return result;
    }
    
    public static int rand_int(int max,int min){
        Random r = new Random();
        int result = r.nextInt((max - min) + 1) + min;
        return result;
    }

    public static double rand_double(double max, double min){
        Random rand = new Random();
        Double result = rand.nextDouble() * (max - min) + min;
        return result;
    }

    public static String rand_bin() {
        Double result = rand_double(1.0,0.0);
        if (result>=0.5) return "1";
        else return "0";
    }
}

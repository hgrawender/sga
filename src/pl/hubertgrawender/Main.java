package pl.hubertgrawender;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\033[0;33m";
    public static final String ANSI_RESET = "\033[0m";

    static double MIN = 0.5;
    static double MAX = 2.5;
    static int Precision = 5;
    static int N_BITS = 0; // number of bits for given precision
    static int M = 200;       // Number of chromosomes
    static int Generations = 100;
    
    static double fitness_sum=0;
    static double Pm = 0.7; //
    static double Pc = 0.1;
    static double Pr = 1 - Pc - Pm;

    static double max_prob = 0;
    static int max_index = 0;
    static double bestX =0;
    
    static double rand_chromosome = 0;
    static double rand_chromosome2 = 0;
    static double rand_method = 0;
    static double round_precision = 1000000.0;

    static List<String> generation = new ArrayList<>();
    static List<Double> decoded_generation = new ArrayList<>();
    static List<Double> generation_fitness = new ArrayList<>();
    static List<Double> gen_probabilities = new ArrayList<>();
    static List<String> output_generation = new ArrayList<>();

    public static void main(String[] args) {

        N_BITS = SGA.calculate_number_of_bits(Precision,MAX,MIN);
        generation = SGA.gen_rand_inputs(M,N_BITS);

        for (int i = 0; i < Generations; i++){
            evolve();

           // System.out.println(Arrays.toString(decoded_generation.toArray()));
            max_prob = Collections.max(gen_probabilities);
           // System.out.println(max_prob);
            max_index = gen_probabilities.indexOf(max_prob);
           bestX = decoded_generation.get(max_index);
          //  System.out.println(bestX+"\t"+ Math.round(FIT.fitness_fun(bestX)*round_precision)/round_precision);
            //System.out.println("P:" + decoded_generation.get(max_index)+" M:"+ Collections.max(decoded_generation));
            if(i < Generations - 1) clean();


//            double max_chromosome = Collections.max(decoded_generation);
//            System.out.println(ANSI_GREEN +(Math.round(max_chromosome*round_precision)/round_precision)+ANSI_RESET);
//            double max_chromosome_value = FIT.fitness_fun(Collections.max(decoded_generation));
//        System.out.println(ANSI_GREEN +(Math.round(max_chromosome_value*round_precision)/round_precision)+ANSI_RESET);

        }
        ///PRINT RESULTS
        System.out.println("Last generation ("+Generations+") was built from: ");
        System.out.println(Arrays.toString(decoded_generation.toArray()));
        System.out.println("Best X: ");
        double max_chromosome = bestX;
        System.out.println(ANSI_GREEN +(Math.round(max_chromosome*round_precision)/round_precision)+ANSI_RESET);
        System.out.println("Value for max X: ");
        double max_chromosome_value = FIT.fitness_fun(bestX);
        System.out.println(ANSI_GREEN +(Math.round(max_chromosome_value*round_precision)/round_precision)+ANSI_RESET);
    }

    public static void evolve(){
        decoded_generation = SGA.create_chromosomes(generation,Precision,N_BITS,MAX,MIN);
        generation_fitness = SGA.calc_fitness_fun(decoded_generation);

        fitness_sum = SGA.fitness_sum(generation_fitness);
        gen_probabilities = SGA.create_roulette(generation_fitness,fitness_sum);


        for (int i=0;i<M;i++){
            do {
                rand_method = SGA.rand_method(Pm,Pc,Pr);        //tak długo aż na końcu nie będzie crossover
            } while (i == M-1 && rand_method == 0.0);

            if(rand_method == 0){
                rand_chromosome = SGA.getRandomInput(gen_probabilities,M);
                rand_chromosome2 = SGA.getRandomInput(gen_probabilities,M);
                List<String> tmp = new ArrayList<>();
                tmp = SGA.crossover(generation.get((int) rand_chromosome),generation.get((int) rand_chromosome2),N_BITS);

                for (int q=0; q<tmp.size(); q++){
                    output_generation.add(tmp.get(q));
                }
                i++;
            }
            else if(rand_method== 1){
                rand_chromosome = SGA.getRandomInput(gen_probabilities,M);
                output_generation.add(SGA.mutation(generation.get((int) rand_chromosome),N_BITS));
            }
            else if(rand_method== 2){
                rand_chromosome = SGA.getRandomInput(gen_probabilities,M);
                output_generation.add(SGA.reproduction(generation.get((int) rand_chromosome)));
            }
        }
        generation=output_generation.stream().collect(Collectors.toList());
    }

    public static void clean(){
        output_generation.clear();
        decoded_generation.clear();
        generation_fitness.clear();
        gen_probabilities.clear();
    }
}


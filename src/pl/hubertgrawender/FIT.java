package pl.hubertgrawender;

public class FIT {
    public static double fitness_fun(double x){
        return ((Math.exp(x) * Math.sin((10*Math.PI*x)) + 1) / x ) + 5;
    }
}
